<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hotel_db";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $room_id = $_POST["room_id"];
    $check_in = $_POST["check_in"];
    $check_out = $_POST["check_out"];
    $guest_name = $_POST["guest_name"];
    $guest_email = $_POST["guest_email"];
    
    $sql = "INSERT INTO reservations (room_id, check_in, check_out, guest_name, guest_email) VALUES ('$room_id', '$check_in', '$check_out', '$guest_name', '$guest_email')";
    
    if ($conn->query($sql) === TRUE) {
        header("Location: reservation.php");
        echo "Reservation created successfully.";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

$conn->close();
?>
