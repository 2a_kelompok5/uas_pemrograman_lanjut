<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Make a Reservation</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <header>
        <h1>Make a Reservation</h1>
        <nav>
        <ul>
        <li><a href="index.html">Home</a></li>
            <li><a href="reservation.php">Make a Reservation</a></li>
            <li><a href="view_reservation.php">View Reservations</a></li>
            <li><a href="info.html">Info</a></li>
            <li><a href="gallery.html">Gallery</a></li>
            <li><a href="contact.html">Contact</a></li>
        </ul>
      </nav>
    </header>
    
    <main>
        <section id="reservation-form">
            <h2>Reservation Form</h2>
            <form action="create_reservation.php" method="POST">
                <label for="room_id">Room Type:</label>
                <select id="room_id" name="room_id" required>
                    <option value="1">Single</option>
                    <option value="2">Twin</option>
                    <option value="3">Double</option>
                    <option value="4">Suite</option>
                </select>

                <label for="check_in">Check-In Date:</label>
                <input type="date" id="check_in" name="check_in" required>
                
                <label for="check_out">Check-Out Date:</label>
                <input type="date" id="check_out" name="check_out" required>
                
                <label for="guest_name">Guest Name:</label>
                <input type="text" id="guest_name" name="guest_name" required>
                
                <label for="guest_email">Guest Email:</label>
                <input type="email" id="guest_email" name="guest_email" required>
                
                <button type="submit">Book Now</button>
            </form>
        </section>
    </main>
    
    <footer>
        <p>kelompok 5</p>
    </footer>
</body>
</html>
