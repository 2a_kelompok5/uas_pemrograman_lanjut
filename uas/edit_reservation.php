<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hotel_db";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $reservation_id = $_POST["reservation_id"];
    $room_id = $_POST["room_id"];
    $check_in = $_POST["check_in"];
    $check_out = $_POST["check_out"];
    $guest_name = $_POST["guest_name"];
    $guest_email = $_POST["guest_email"];

    $sql = "UPDATE reservations SET room_id='$room_id', check_in='$check_in', check_out='$check_out', guest_name='$guest_name', guest_email='$guest_email' WHERE reservation_id='$reservation_id'";

    if ($conn->query($sql) === TRUE) {
        header("Location: view_reservation.php");
        echo "Reservation updated successfully.";
    } else {
        echo "Error updating reservation: " . $conn->error;
    }
}

$reservation_id = $_GET ["reservation_id"];
$sql = "SELECT * FROM reservations WHERE reservation_id ='$reservation_id'";
$result = $conn->query($sql);

$reservation = null;
if ($result->num_rows > 0) {
    $reservation = $result->fetch_assoc();
}
$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Reservation</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <header>
        <h1>Edit Reservation</h1>
        <nav>
            <ul>
                <li><a href="index.html">Home</a></li>
                <li><a href="reservation.php">Make a Reservation</a></li>
                <li><a href="view_reservation.php">View Reservations</a></li>
                <li><a href="info.html">Info</a></li>
                <li><a href="gallery.html">Gallery</a></li>
                <li><a href="contact.html">Contact</a></li>
            </ul>
        </nav>
    </header>
    
    <main>
        <?php if ($reservation) { ?>
            <form action="edit_reservation.php" method="POST">
                <input type="hidden" name="reservation_id" value="<?php echo $reservation['reservation_id']; ?>">
                <label for="room_id">Room Type:</label>
                <select id="room_id" name="room_id" required>
                    <option value="1" <?php if ($reservation['room_id'] == 1) echo 'selected'; ?>>Single</option>
                    <option value="2" <?php if ($reservation['room_id'] == 2) echo 'selected'; ?>>Twin</option>
                    <option value="3" <?php if ($reservation['room_id'] == 3) echo 'selected'; ?>>Double</option>
                    <option value="4" <?php if ($reservation['room_id'] == 4) echo 'selected'; ?>>Suite</option>
                </select>

                <label for="check_in">Check-In Date:</label>
                <input type="date" id="check_in" name="check_in" required value="<?php echo $reservation['check_in']; ?>">
                
                <label for="check_out">Check-Out Date:</label>
                <input type="date" id="check_out" name="check_out" required value="<?php echo $reservation['check_out']; ?>">
                
                <label for="guest_name">Guest Name:</label>
                <input type="text" id="guest_name" name="guest_name" required value="<?php echo $reservation['guest_name']; ?>">
                
                <label for="guest_email">Guest Email:</label>
                <input type="email" id="guest_email" name="guest_email" required value="<?php echo $reservation['guest_email']; ?>">
                
                <button type="submit">Update Reservation</button>
            </form>
        <?php } else { ?>
            <p>Reservation not found.</p>
        <?php } ?>
    </main>
    
    <footer>
        <p>kelompok 5</p>
    </footer>
</body>
</html>
