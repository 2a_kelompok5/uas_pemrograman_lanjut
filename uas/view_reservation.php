<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hotel_db";

$conn = new mysqli($servername, $username, $password, $dbname);


if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$sql = "SELECT * FROM reservations";
$result = $conn->query($sql);

$reservations = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $reservations[] = $row;
    }
}
$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>View Reservations</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <header>
        <h1>Reservations</h1>
        <nav>
        <ul>
        <li><a href="index.html">Home</a></li>
            <li><a href="reservation.php">Make a Reservation</a></li>
            <li><a href="view_reservation.php">View Reservations</a></li>
            <li><a href="info.html">Info</a></li>
            <li><a href="gallery.html">Gallery</a></li>
            <li><a href="contact.html">Contact</a></li>
        </ul>
      </nav>
    </header>
    
    <main>
        <table>
            <tr>
                <th>Reservation ID</th>
                <th>Room Type</th>
                <th>Check-In</th>
                <th>Check-Out</th>
                <th>Guest Name</th>
                <th>Guest Email</th>
            </tr>
            <?php foreach ($reservations as $reservation) { ?>
                <tr>
                    <td><?php echo $reservation['reservation_id']; ?></td>
                    <td><?php echo $reservation['room_id']; ?></td>
                    <td><?php echo $reservation['check_in']; ?></td>
                    <td><?php echo $reservation['check_out']; ?></td>
                    <td><?php echo $reservation['guest_name']; ?></td>
                    <td><?php echo $reservation['guest_email']; ?></td>
                    <td>
                        <a href="edit_reservation.php?reservation_id=<?php echo $reservation['reservation_id']; ?>">Edit</a>
                        <a href="delete_reservation.php?reservation_id=<?php echo $reservation['reservation_id']; ?>">Delete</a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </main>
    
    <footer>
        <p>kelompok 5</p>
    </footer>
</body>
</html>
