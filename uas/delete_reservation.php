<?php
// Menghubungkan ke database
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hotel_db";

$conn = new mysqli($servername, $username, $password, $dbname);

// Memeriksa koneksi
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Memeriksa apakah parameter reservation_id ada
if (isset($_GET['reservation_id'])) {
    $reservation_id = $_GET['reservation_id'];

    // Query SQL untuk menghapus reservasi berdasarkan ID
    $sql = "DELETE FROM reservations WHERE reservation_id = '$reservation_id'";

    if ($conn->query($sql) === TRUE) {
        header("Location: view_reservation.php");
        echo "Reservation deleted successfully.";
    } else {
        echo "Error deleting reservation: " . $conn->error;
    }
} else {
    echo "Invalid reservation ID.";
}

$conn->close();
?>
